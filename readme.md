# matrix-site
An example site sourcing its content from a
[matrix.org](https://matrix.org) room and custom events, using
[@sctlib/mwc](https://gitlab.com/sctlib/mwc) web-components (imported
from the [unpkg](https://unpkg.com) CDN).

# How it works
The dynamic content of this site is sourced from a public matrix room,
by its matrix alias address `#mwc-chat:matrix.org` using the custom
event type `io.gitlab.ugrp.post`.

To discover in more details how it is built, to customize and edit for
other web-pages, look into the `index.html` file and its comments (as
well as into the `mwc` documentation, examples and specs).

It is written in idiomatic web languages, HTML, Javascript and CSS, to
describe which content is loaded from the room, how new one can be
created, and how all should be displayed.

# Development
To edit the project locally on any device:
- download/clone/fork this project's repository (or copy the HTML)
- open the `index.html` file in a web browser

Alternatively, run a local server in the current folder, for example
with the `npx serve .` or `python3 -m http.server --directory .`
commands.

All there is to edit is in `index.html` (which could be modularized
into more HTML, CSS or JS files; here left in one file for a complete
example overview).

## Customize and edit
To setup matrix custom event types, we can edit the HTML elements and
attributes of the web-components `matrix-*` as well as custom
`displayTemplate` and `formTemplate`. For more information see the
`mwc` documention and matrix user group.


## Deployment
This site is deployed as a gitlab static `page` by the job described
in the `.gitlab-ci.yml` file.
